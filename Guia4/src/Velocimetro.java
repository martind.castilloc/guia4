
public class Velocimetro {
	
	private int velocidad;
	private int velocidad_maxima = 120;
	private int distancia;
	
	Velocimetro(){
		this.velocidad = 0;
		this.distancia = 0;
	}
	
	public void setVelocidad(int a){
		
		if(this.velocidad + a >= velocidad_maxima){
			System.out.println("Ya ha alcanzado la velocidad maxima!");
			this.velocidad = velocidad_maxima;
		}else {
			this.velocidad = this.velocidad + a;
		}
	}
	
	
	public int getVelocidad() {
		return this.velocidad;
	}
	
	
	public void setDistancia(int a) {
		this.distancia = this.distancia + a;
	}
	
	public double getDistancia() {
		return this.distancia;
	}

}
