import java.util.Scanner;
import java.util.Random;

public class Controlador {
	
	private int velocidad;
	private int tiempo;
	
	Controlador(){
		
		Automovil a = new Automovil();	
		
		Motor m1 = new Motor();
		a.setMotor(m1);
		
		Estanque e = new Estanque();
		a.setEstanque(e);
		
		Velocimetro v = new Velocimetro();
		a.setVelocimetro(v);
		
		Scanner sc = new Scanner(System.in);
		Random rd = new Random();
				
		a.getMotor().getCilindrada();
		for (int i=0; i<4;i++) {
			a.setRueda(new Rueda());
		}
		
		a.enciende_automovil();
		
		
		while(a.getEstanque().getGasolina() > 0.0) {
			while(a.getEncendido()) {
				while(a.getEstanque().getGasolina() > 0.0) {
					System.out.println("Presione 4 para que el auto se mueva");
					int opcion = sc.nextInt();	
					System.out.print("\n");
					if(opcion == 4 && a.getEstanque().getGasolina() > 0.0) {
						velocidad = rd.nextInt(30) + 1;
						v.setVelocidad(velocidad);
						tiempo = rd.nextInt(3) + 1;
						a.getVelocimetro().setDistancia(a.getVelocimetro().getVelocidad() * tiempo);
						double distancia_temp = a.getVelocimetro().getVelocidad() * tiempo;
						double consumo = ((a.getVelocimetro().getVelocidad() * a.getMotor().get_kml()) / 120);
						double consumo_total = (distancia_temp / consumo);
						a.getEstanque().setGasolina(consumo_total);
						distancia_temp = 0;
						int desgaste = rd.nextInt(10) + 1;
						for (int i = 0; i<4; i++) {
							a.getRueda().get(i).setIntegridad(desgaste);
							if (rd.nextInt(10) == 0) {
								a.getRueda().get(i).setIntegridad(rd.nextInt(20)+1);
							}
						}
						a.reporte_automovil();
					}
						a.apagar();		
				}	
			}
		}	
	}
}

