import java.util.ArrayList;
import java.util.Random;

public class Automovil {

	private Motor motor;
	private Velocimetro velocimetro;
	private ArrayList<Rueda> rueda;
	private Estanque estanque;
	private Boolean encendido ;
	Random rd = new Random();
	
	Automovil(){
		this.rueda = new ArrayList<Rueda>();
		this.encendido = false;
	}
	
	public Motor getMotor() {
		return motor;
	}
	
	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public Velocimetro getVelocimetro() {
		return velocimetro;
	}

	public void setVelocimetro(Velocimetro velocimetro) {
		this.velocimetro = velocimetro;
	}

	public Estanque getEstanque() {
		return estanque;
	}

	public void setEstanque(Estanque estanque) {
		this.estanque = estanque;
	}

	public Boolean getEncendido() {
		return encendido;
	}

	public void setEncendido(Boolean encendido) {
		this.encendido = encendido;
	}

	public ArrayList<Rueda> getRueda() {
		return rueda;
	}

	public void setRueda(Rueda rueda) {
		this.rueda.add(rueda);
	}
	
	
	public void enciende_automovil() {
		this.encendido = true;	
		estanque.setGasolina(estanque.getGasolina()/100);
		System.out.println("Se encendio el auto");
	}
	

	public void revicion_ruedas() {
		System.out.println("Se baja del auto para hacer un chequeo de los neumaticos");
     for (int i = 0; i < 4 ; i++) {
     	if (getRueda().get(i).getIntegridad() <= 10) {
     		System.out.println("Al pareecer la rueda " + (i + 1) + " debe ser cambiada");
     		System.out.print("Debo apagar el vehiculo");
     		apagar();
     		System.out.print("Se ha cambiado el neumatico");
     		enciende_automovil();
     	}
	
     	else {
     		if (i == 3) {
     			System.out.println("Las ruedas se encuentran en buenas condiciones");	
     			}
     		}						
     	}
	}
	
	
	public void reporte_automovil() {
		System.out.println("La velocidad actual del automovil es " + velocimetro.getVelocidad() + "Km/h");
		System.out.println("a recorrido " + getVelocimetro().getDistancia() + " Kilometros");
		System.out.println("El estado actual del estanque es: " + this.estanque.getGasolina() + "%");
		revicion_ruedas();
		System.out.println(
				getRueda().get(0).getIntegridad() + "% | " +
                getRueda().get(1).getIntegridad() + "% | " +
				getRueda().get(2).getIntegridad() + "% | " +
                getRueda().get(3).getIntegridad() + "%"
                );
	}
	
	public void apagar() {
		System.out.print("\nEl vehiculo se ha apagado\n");
		this.encendido = false;
	}
	
	
}
