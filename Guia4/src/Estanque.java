
public class Estanque {


		private Double gasolina;
			
		Estanque(){
			this.gasolina = 100.0;
		}
		
		public void setGasolina(Double consumo){
			if((this.gasolina - consumo) > 0.0) {
				this.gasolina = this.gasolina - consumo;	
			}
			else {
				this.gasolina = 0.0;
				System.out.println("SE ACABO EL COMBUSTIBLE");
			}
		}
		
		public Double getGasolina() {
			return this.gasolina;
		}
		
}

