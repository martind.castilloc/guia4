import java.util.Random;

public class Motor {

	private String cilindrada;
	private double Km;
	
	Motor(){
		/* Random para determinar cilindrada */
		String cilindrada[] = new String[]{"1,2", "1,6"};
		this.cilindrada = cilindrada[new Random().nextInt(cilindrada.length)];
		if(this.cilindrada == "1,2") {
			this.Km = 20;
		}else {
			this.Km = 14;
		}
	}
	
	public String getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(String cilindrada) {
		this.cilindrada = cilindrada;
	}
	
	
	public double get_kml() {
		return this.Km;
	}
	
	
}
